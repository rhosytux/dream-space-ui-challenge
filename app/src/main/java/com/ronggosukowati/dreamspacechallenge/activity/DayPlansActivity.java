package com.ronggosukowati.dreamspacechallenge.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.ronggosukowati.dreamspacechallenge.R;
import com.ronggosukowati.dreamspacechallenge.adapter.ListCityAdapter;
import com.ronggosukowati.dreamspacechallenge.data.CitiesData;
import com.ronggosukowati.dreamspacechallenge.model.City;

import java.util.ArrayList;

public class DayPlansActivity extends AppCompatActivity {

    private RecyclerView rvCities, rvMainSights;
    private Button btnExpand;
    private LinearLayout expandView;
    private ArrayList<City> cities = new ArrayList<>();
    private ArrayList<City> mainSights = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_plans);

        initViews();
        initToolbar();

        cities.addAll(CitiesData.getCities());
        showRecyclerViewList();

        mainSights.addAll(CitiesData.getCities());
        showMainSights();

        btnExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (expandView.getVisibility() == View.GONE){
                    expandView.setVisibility(View.VISIBLE);
                    btnExpand.setBackgroundResource(R.drawable.ic_keyboard_arrow_up);
                }else {
                    expandView.setVisibility(View.GONE);
                    btnExpand.setBackgroundResource(R.drawable.ic_keyboard_arrow_down);
                }
            }
        });

    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Plans");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void initViews() {
        rvCities = findViewById(R.id.rv_cities);
        rvCities.setHasFixedSize(true);
        rvMainSights = findViewById(R.id.rv_main_sights);
        rvMainSights.setHasFixedSize(true);
        btnExpand = findViewById(R.id.btn_expand);
        expandView = findViewById(R.id.expand_view);
    }

    private void showRecyclerViewList() {
        rvCities.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        ListCityAdapter citiesAdapter = new ListCityAdapter(cities);
        rvCities.setAdapter(citiesAdapter);
    }

    private void showMainSights() {

        rvMainSights.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        ListCityAdapter citiesAdapter = new ListCityAdapter(mainSights);
        rvMainSights.setAdapter(citiesAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);

    }
}
