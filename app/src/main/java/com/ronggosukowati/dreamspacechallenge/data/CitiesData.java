package com.ronggosukowati.dreamspacechallenge.data;

import com.ronggosukowati.dreamspacechallenge.model.City;

import java.util.ArrayList;

public class CitiesData {

    public static String[][] data = new String[][]{
            {"https://images.pexels.com/photos/342113/pexels-photo-342113.jpeg", "Paris 1"},
            {"https://images.pexels.com/photos/705764/pexels-photo-705764.jpeg", "Paris 2"},
            {"https://images.pexels.com/photos/1060791/pexels-photo-1060791.jpeg", "Paris 3"},
            {"https://images.pexels.com/photos/2574631/pexels-photo-2574631.jpeg", "Paris 4"},
            {"https://images.pexels.com/photos/2130611/pexels-photo-2130611.jpeg", "Paris 5"}

    };

    public static ArrayList<City> getCities(){
        ArrayList<City> list = new ArrayList<>();
        for (String[] aData : data) {
            City city = new City();
            city.setImage(aData[0]);
            city.setDesc(aData[1]);

            list.add(city);
        }

        return list;
    }
}
