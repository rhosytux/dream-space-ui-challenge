package com.ronggosukowati.dreamspacechallenge.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ronggosukowati.dreamspacechallenge.R;
import com.ronggosukowati.dreamspacechallenge.model.City;

import java.util.ArrayList;

public class ListCityAdapter extends RecyclerView.Adapter<ListCityAdapter.ViewHolder> {

    private ArrayList<City> cities;

    public ListCityAdapter(ArrayList<City> cities) {
        this.cities = cities;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        City city = cities.get(position);
        Glide.with(holder.itemView.getContext())
                .load(city.getImage())
                .into(holder.imgCity);

    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgCity;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imgCity = itemView.findViewById(R.id.img_city);
        }
    }
}
